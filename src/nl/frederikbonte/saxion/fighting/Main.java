package nl.frederikbonte.saxion.fighting;

import nl.frederikbonte.saxion.fighting.utils.SaxionAppIOSystem;
import nl.saxion.app.SaxionApp;

import java.io.IOException;

public class Main implements Runnable {
    public static void main(String[] args) {
        System.out.println("Hello world!");

        SaxionApp.start(new Main(), 800, 600);
    }

    @Override
    public void run() {
        SaxionApp.print("Hello SaxionApp!");
        SaxionAppIOSystem ioSystem = new SaxionAppIOSystem();
        Adventure adventure;
        try
        {
            adventure = new Adventure("/adventure.csv");
        }
        catch (IOException e)
        {
            System.err.println("Failed to read adventure : "+e.getMessage());
            e.printStackTrace();
            return;
        }
        Character brock = new Character("Brock");
        adventure.startAdventure(brock);
        for (int i = 0; i < 10; i++)
        {
            SaxionApp.clear();
            ioSystem.printCharacter(brock);
            ioSystem.println();
            ioSystem.println(brock.getStoryElement().getText());
            ioSystem.println(" - - - - - - - - ");
            brock.getStoryElement().showMenu(ioSystem, brock);
        }
        ioSystem.println("No more actions for today...");
    }
}