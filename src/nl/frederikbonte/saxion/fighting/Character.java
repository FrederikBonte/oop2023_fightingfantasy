package nl.frederikbonte.saxion.fighting;

import nl.frederikbonte.saxion.fighting.items.Item;
import nl.frederikbonte.saxion.fighting.utils.RandomSource;
import nl.frederikbonte.saxion.fighting.utils.Score;

import java.util.ArrayList;

public class Character
{
    // Name of the character.
    private String name;
    // Three ability scores as defined by Fighting Fantasy.
    private Score skill, stamina, luck;
    // Primary items, for now one can only equip one of each.
    private Item weapon, armor, shield;
    private StoryElement element = null;
    private final ArrayList<Item> items = new ArrayList<>();

    public Character(String name, int skill, int stamina, int luck)
    {
        this.name = name;
        this.skill = new Score(skill);
        this.stamina = new Score(stamina);
        this.luck = new Score(luck);
    }

    public Character(String name)
    {
        this(
                name,
                RandomSource.roll(1, 6),
                RandomSource.roll(2, 12),
                RandomSource.roll(1, 6)
        );
    }

    public String getName()
    {
        return name;
    }

    //<editor-fold desc="Ability score functions">
    public int getCurrentSkill()
    {
        return this.skill.getValue();
    }

    public int getInitialSkill()
    {
        return this.skill.getMaxValue();
    }

    public int getCurrentStamina()
    {
        return this.stamina.getValue();
    }

    public int getInitialStamina()
    {
        return this.stamina.getMaxValue();
    }

    public int getCurrentLuck()
    {
        return this.luck.getValue();
    }

    public int getInitialLuck()
    {
        return this.luck.getMaxValue();
    }

    /**
     * Retrieves the skill {@link Score} for the {@link GameRules} funtions.
     * @return the skill {@link Score} for the {@link GameRules} funtions.
     */
    Score getSkill()
    {
        return skill;
    }

    /**
     * Retrieves the stamina {@link Score} for the {@link GameRules} funtions.
     * @return the stamina {@link Score} for the {@link GameRules} funtions.
     */
    Score getStamina()
    {
        return stamina;
    }

    /**
     * Retrieves the luck {@link Score} for the {@link GameRules} funtions.
     * @return the luck {@link Score} for the {@link GameRules} funtions.
     */
    Score getLuck()
    {
        return luck;
    }
    //</editor-fold>

    public boolean isAlive()
    {
        return !isDead();
    }

    public boolean isDead()
    {
        return this.stamina.getValue() <= 0;
    }

    public StoryElement getStoryElement()
    {
        return element;
    }

    public void setStoryElement(StoryElement element)
    {
        this.element = element;
    }

    //<editor-fold desc="Primary equipment">
    public Item getWeapon()
    {
        return weapon;
    }

    public Item getArmor()
    {
        return armor;
    }

    public Item getShield()
    {
        return shield;
    }

    /**
     * Attempt to equip the provided item. Not all {@link nl.frederikbonte.saxion.fighting.items.Type}s
     * of {@link Item}s can be equipped. Also one cannot equip two swords for instance.
     * @param item the {@link Item} that is equipped.
     * @return <i>false</i> when the provided {@link Item} cannot be equipped.
     */
    public boolean equip(Item item)
    {
        switch (item.getType())
        {
            case WEAPON -> { return equipWeapon(item); }
            case SHIELD -> { return equipShield(item); }
            case ARMOR ->  { return equipArmor(item); }
            default -> { return false; }
        }
    }

    /**
     * Attempt to unequip an {@link Item}. This requires that particular item to be equipped.
     * @param item the {@link Item} to unequip.
     * @return <i>false</i> when the provided {@link Item} is not the one equipped.
     */
    public boolean unequip(Item item)
    {
        switch (item.getType())
        {
            case WEAPON -> { return unequipWeapon(item); }
            case SHIELD -> { return unequipShield(item); }
            case ARMOR ->  { return unequipArmor(item); }
            default -> { return false; }
        }
    }
    //</editor-fold>

    //<editor-fold desc="Functions for equipping and unequipping items.">
    private boolean equipWeapon(Item item)
    {
        if (weapon==null)
        {
            weapon = item;
            return true;
        }
        else
        {
            return false;
        }
    }

    private boolean equipShield(Item item)
    {
        if (shield==null)
        {
            shield = item;
            return true;
        }
        else
        {
            return false;
        }
    }

    private boolean equipArmor(Item item)
    {
        if (armor==null)
        {
            armor = item;
            return true;
        }
        else
        {
            return false;
        }
    }

    private boolean unequipWeapon(Item item)
    {
        if (weapon==null || !weapon.equals(item))
        {
            return false;
        }
        else
        {
            weapon = null;
            return true;
        }
    }

    private boolean unequipShield(Item item)
    {
        if (shield==null || !shield.equals(item))
        {
            return false;
        }
        else
        {
            shield = null;
            return true;
        }
    }

    private boolean unequipArmor(Item item)
    {
        if (armor==null || !armor.equals(item))
        {
            return false;
        }
        else
        {
            armor = null;
            return true;
        }
    }

    public void add(Item item)
    {
        this.items.add(item);
    }

    public boolean hasItem(Item item)
    {
        for (Item i:items)
        {
            if (i.equals(item))
            {
                return true;
            }
        }
        return false;
    }
    //</editor-fold>
}