package nl.frederikbonte.saxion.fighting;

import nl.frederikbonte.saxion.fighting.items.*;
import nl.frederikbonte.saxion.fighting.actions.EffectRestriction;
import nl.frederikbonte.saxion.fighting.utils.RandomSource;
import nl.frederikbonte.saxion.fighting.utils.Score;

public class GameRules
{
	//<editor-fold desc="List of known effects and the scores that they influence.">
	public static final AffectedScoreRetriever RETRIEVE_STAMINA = new AffectedScoreRetriever()
	{
		@Override
		public Score getScore(Character character)
		{
			return character.getStamina();
		}
	};

	public static final AffectedScoreRetriever RETRIEVE_SKILL = new AffectedScoreRetriever()
	{
		@Override
		public Score getScore(Character character)
		{
			return character.getSkill();
		}
	};

	public static final AffectedScoreRetriever RETRIEVE_LUCK = new AffectedScoreRetriever()
	{
		@Override
		public Score getScore(Character character)
		{
			return character.getLuck();
		}
	};

	public static final ChangeScoreEffect HARM = new ChangeScoreEffect()
	{
		@Override
		public void change(Score score, int value)
		{
			score.decrease(value);
		}
	};

	public static final ChangeScoreEffect RESTORE = new ChangeScoreEffect()
	{
		@Override
		public void change(Score score, int value)
		{
			score.increase(value);
		}
	};

	public static final ChangeScoreEffect RAISE = new ChangeScoreEffect()
	{
		@Override
		public void change(Score score, int value)
		{
			score.raise(value);
		}
	};

	public static final EffectRestriction NO_RESTRICTION = EffectRestriction.NO_RESTRICTION;

	public static final Equippable DEFAULT_EQUIPPABLE = DefaultEquippable.INSTANCE;

	//</editor-fold>

	/**
	 * Reduces the current stamina of a {@link Character} by the provided amount.
	 * @param c the {@link Character} that is influenced.
	 * @param damage the amount of damage taken.
	 */
	public static void takeDamage(Character c, int damage)
	{
		c.getStamina().decrease(damage);
	}

	/**
	 * Attempt to use luck to influence outcomes. (See the book.)
	 * @param c the {@link Character} that uses luck.
	 * @return <i>true</i> when the {@link Character} succeeds.
	 */
	public static boolean useLuck(Character c)
	{
		if (c.getLuck().getValue() == 0)
		{
			return false;
		}
		else
		{
			boolean result = RandomSource.roll(2) <= c.getLuck().getValue();
			c.getLuck().decrease(1);
			return result;
		}
	}


	/**
	 * Attempt to consume an item. Will throw {@Link UnsupporedOperationException}s when the character is not
	 * allowed to consume the item, or when the item is not consumable. All these checks can (and should) be
	 * checked before.
	 * @param character the {@link Character} that consumes the item.
	 * @param item the {@link Item} that is (hopefully) intended for consumption.
	 */
	public static void consume(Character character, Item item)
	{
		if (!item.getConsumable().isConsumable())
		{
			throw new UnsupportedOperationException("Cannot consume "+item.getName());
		}
		else if (!item.getConsumable().canConsume(character))
		{
			throw new UnsupportedOperationException(character.getName()+" is not allowed to consume "+item.getName());
		}
		else
		{
			// @TODO: Remove from inventory.
			//character.getInventory().remove(item);
			// Apply the effect of consumption on the character...
			item.getConsumable().consume(character);
		}
	}

	/**
	 * Attempt to equip an item. Will throw {@Link UnsupporedOperationException}s when the character is not
	 * allowed to equip the item, or when the item is not equippable. All these checks can (and should) be
	 * checked before.
	 * @param character the {@link Character} that equips the item.
	 * @param item the {@link Item} that is (hopefully) equippable.
	 */
	public static void equip(Character character, Item item)
	{
		if (!item.getEquippable().isEquippable())
		{
			throw new UnsupportedOperationException("Cannot equip "+item.getName());
		}
		else if (!item.getEquippable().canEquip(character))
		{
			throw new UnsupportedOperationException(character.getName()+" is not allowed to equip "+item.getName());
		}
		else
		{
			if (character.equip(item))
			{
				item.getEquippable().equip(character);
			}
			else
			{
				throw new UnsupportedOperationException(character.getName()+" already has another "+item.getType().name().toLowerCase()+" equipped.");
			}
		}
	}

	/**
	 * Attempt to unequip an item. Will throw {@Link UnsupporedOperationException}s when the character is not
	 * allowed to unequip the item, or when the item is not (un/)equippable. All these checks can (and should) be
	 * checked before.
	 * @param character the {@link Character} that equips the item.
	 * @param item the {@link Item} that is presumable currently equipped.
	 */
	public static void unequip(Character character, Item item)
	{
		if (!item.getEquippable().isEquippable())
		{
			throw new UnsupportedOperationException("Cannot unequip "+item.getName());
		}
		else if (!item.getEquippable().canEquip(character))
		{
			throw new UnsupportedOperationException(character.getName()+" is not allowed to unequip "+item.getName());
		}
		else
		{
			if (character.unequip(item))
			{
				// Apply the effects of equipping this weapon.
				item.getEquippable().unequip(character);
			}
			else
			{
				throw new UnsupportedOperationException(character.getName()+" is not currently equipping "+item.getName());
			}
		}
	}

	public static void move(Character character, StoryElement element)
	{
		character.setStoryElement(element);
	}
}
