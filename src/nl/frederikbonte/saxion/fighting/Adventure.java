package nl.frederikbonte.saxion.fighting;

import nl.frederikbonte.saxion.fighting.items.DefaultEquippable;
import nl.frederikbonte.saxion.fighting.items.Item;
import nl.frederikbonte.saxion.fighting.items.Type;
import nl.frederikbonte.saxion.fighting.actions.Action;
import nl.frederikbonte.saxion.fighting.actions.CharacterHasItemRestriction;
import nl.frederikbonte.saxion.fighting.actions.EffectRestriction;
import nl.frederikbonte.saxion.fighting.actions.MoveEffect;
import nl.frederikbonte.saxion.fighting.utils.ItemReader;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.ParseException;
import java.util.HashMap;

public class Adventure
{
	final HashMap<Integer, StoryElement> elements = new HashMap<>();
	final HashMap<Integer, Item> items = new HashMap<>();

	public Adventure(String resource) throws IOException
	{
		BufferedReader reader = new BufferedReader(new InputStreamReader(ItemReader.class.getResourceAsStream(resource)));
		String line;
		while ((line=reader.readLine())!=null)
		{
			line = line.trim();
			if (line.length()==0 || line.startsWith("//") || line.startsWith("#"))
			{
				continue;
			}
			try
			{
				readAdventureLine(line.split(";"));
			} catch (Exception ex)
			{
				System.err.println("Failed to read : "+line+" because : "+ex.getMessage());
			}
		}
	}

	public void startAdventure(Character character)
	{
		character.setStoryElement(elements.get(1));
		StoryElement init = elements.get(0);
		for (Item item:init.items.values())
		{
			if (!character.equip(item))
			{
				character.add(item);
			}
		}
	}

	public StoryElement getElement(int id)
	{
		return elements.get(id);
	}

	public Item getItem(int id) { return this.items.get(id); }

	private void readAdventureLine(String[] elements) throws ParseException
	{
		if (elements[0].equals("STORY"))
		{
			readStoryElement(elements);
		}
		else if (elements[0].equals("ITEM"))
		{
			readItem(elements);
		}
		else if (elements[0].equals("ACTION"))
		{
			readAction(elements);
		}
		else
		{
			throw new ParseException("Unexpected line type : "+elements[0], 0);
		}
	}

	private void readItem(String[] elements)
	{
		int storyId = parseId(elements[1]);
		int itemId = parseId(elements[2]);
		String name = elements[3];
		String description = elements[4];
		Type type = Type.valueOf(elements[5]);
		Item item;

		if (type==Type.POTION)
		{
			item = ItemReader.createConsumable(itemId, name, description, Type.POTION, ItemReader.getEffect(elements[6]), ItemReader.getScore(elements[7]), ItemReader.parseValue(elements[8]));
		}
		else if (type==Type.PROVISION)
		{
			item = ItemReader.createConsumable(itemId, name, description, Type.PROVISION, ItemReader.getEffect(elements[6]), ItemReader.getScore(elements[7]), ItemReader.parseValue(elements[8]));
		}
		else if (type==Type.WEAPON || type==Type.ARMOR || type==Type.SHIELD)
		{
			if (elements[6].equalsIgnoreCase("NONE"))
			{
				item = new Item(itemId, name, description, type, DefaultEquippable.INSTANCE);
			}
			else
			{
				item = ItemReader.createMagicEquippable(itemId, name, description, type, ItemReader.getScore(elements[7]), ItemReader.parseValue(elements[8]));
			}
		}
		else {
			item = new Item(itemId, name, description, type);
		}

		this.items.put(item.getId(), item);
		this.elements.get(storyId).add(item);
	}

	private void readAction(String[] elements)
	{
		int storyId = parseId(elements[1]);
		String text = elements[2];
		String type = elements[3];
		Action action;
		if (type.equals("MOVE"))
		{
			EffectRestriction restriction = createRestriction(elements, 5);
			action = createMoveAction(text, parseId(elements[4]), restriction);
		}
		else
		{
			throw new IllegalArgumentException("Unknown action type : "+type);
		}

		this.elements.get(storyId).add(action);
	}

	private EffectRestriction createRestriction(String[] elements, int offset)
	{
		if (elements.length<=offset)
		{
			return GameRules.NO_RESTRICTION;
		}
		String type = elements[offset];
		if (type.equals("HAS"))
		{
			int itemId = parseId(elements[offset+1]);
			return new CharacterHasItemRestriction(this, itemId);
		}
		else
		{
			throw new IllegalArgumentException("Unknown restriction type : "+type);
		}
	}

	private Action createMoveAction(String text, int targetId, EffectRestriction restriction)
	{
		return new Action(text, new MoveEffect(this, targetId, restriction));
	}

	public static int parseId(String value)
	{
		if (value.equalsIgnoreCase("MAX"))
		{
			return Integer.MAX_VALUE;
		}
		else
		{
			return Integer.parseInt(value);
		}
	}

	private void readStoryElement(String[] elements)
	{
		int id = parseId(elements[1]);
		String text = elements[2];
		this.elements.put(id, new StoryElement(id, text));
	}
}
