package nl.frederikbonte.saxion.fighting;

import nl.frederikbonte.saxion.fighting.items.Item;
import nl.frederikbonte.saxion.fighting.actions.Action;
import nl.frederikbonte.saxion.fighting.actions.TakeEffect;
import nl.frederikbonte.saxion.fighting.utils.AbstractIOSystem;

import java.util.ArrayList;
import java.util.HashMap;

public class StoryElement
{
	private final int id;
	private final String text;
	private final String image;

	final HashMap<Integer, Item> items = new HashMap<>();
	final ArrayList<Action> actions = new ArrayList<>();

	public StoryElement(int id, String text, String image)
	{
		this.id = id;
		this.text = text;
		this.image = image;
	}

	public StoryElement(int id, String text)
	{
		this(id, text, null);
	}

	public int getId()
	{
		return id;
	}

	public String getText()
	{
		return text;
	}

	public String getImage()
	{
		return image;
	}

	public void add(Item item)
	{
		this.items.put(item.getId(), item);
	}

	public Item getItem(int itemId)
	{
		return this.items.get(itemId);
	}

	public Item remove(int itemId)
	{
		return this.items.remove(itemId);
	}

	void add(Action action)
	{
		this.actions.add(action);
	}

	public void showMenu(AbstractIOSystem ioSystem, Character character)
	{
		ArrayList<Action> menu = new ArrayList<>();
		for (Action action:actions)
		{
			menu.add(action);
		}
		for (Item item:items.values())
		{
			menu.add(new Action("Take "+item.getName(), new TakeEffect(this, item.getId())));
		}
		Action action = ioSystem.printMenu(menu);
		if (action==null)
		{
			// User didn't choose an action to execute...
		}
		else if (action.getEffect().getRestriction().canTrigger(character))
		{
			// User chose an action that the character is allowed to execute.
			action.getEffect().execute(character);
		}
		else
		{
			// The character is not allowed to take this action.
			ioSystem.println(character.getName()+" is not allowed to do that.");
			ioSystem.pause();
		}
	}
}
