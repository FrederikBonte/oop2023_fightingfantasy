package nl.frederikbonte.saxion.fighting.utils;

import nl.saxion.app.SaxionApp;

public class SaxionAppIOSystem extends AbstractIOSystem
{
	@Override
	public void print(String text)
	{
		SaxionApp.print(text);
	}

	@Override
	public void println(String text)
	{
		SaxionApp.printLine(text);
	}

	@Override
	public int readInteger()
	{
		return SaxionApp.readInt();
	}

	@Override
	protected String readText()
	{
		return SaxionApp.readString();
	}

	@Override
	public void pause()
	{
		SaxionApp.pause();
	}
}
