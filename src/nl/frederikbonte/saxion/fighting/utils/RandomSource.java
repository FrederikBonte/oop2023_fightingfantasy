package nl.frederikbonte.saxion.fighting.utils;

import java.util.Random;

public class RandomSource {
    protected static Random RNG = new Random();

    public static final int roll(int number, int modifier)
    {
        int result = modifier;
        for (int i = 0; i < number; i++) {
            result += RNG.nextInt(1,7);
        }
        return result;
    }

    public static final int roll(int number)
    {
        return roll(number, 0);
    }

    public static final int roll1D6()
    {
        return roll(1,0);
    }
}
