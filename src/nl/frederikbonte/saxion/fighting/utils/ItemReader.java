package nl.frederikbonte.saxion.fighting.utils;

import nl.frederikbonte.saxion.fighting.GameRules;
import nl.frederikbonte.saxion.fighting.items.*;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class ItemReader
{
	public final ArrayList<Item> items = new ArrayList<>();

	public ItemReader(String resource) throws IOException
	{
		BufferedReader reader = new BufferedReader(new InputStreamReader(ItemReader.class.getResourceAsStream(resource)));
		String line;
		while ((line=reader.readLine())!=null)
		{
			line = line.trim();
			if (line.length()==0 || line.startsWith("//") || line.startsWith("#"))
			{
				continue;
			}
			try
			{
				Item item = readItem(line.split(";"));
				items.add(item);
			} catch (Exception ex)
			{
				System.err.println("Failed to read : "+line+" because : "+ex.getMessage());
			}
		}
	}

	private Item readItem(String[] elements)
	{
		int itemId = Integer.parseInt(elements[0]);
		String name = elements[1];
		String description = elements[2];
		Type type = Type.valueOf(elements[3]);

		if (type==Type.POTION)
		{
			return createConsumable(itemId, name, description, Type.POTION, getEffect(elements[4]), getScore(elements[5]), parseValue(elements[6]));
		}
		else if (type==Type.PROVISION)
		{
			return createConsumable(itemId, name, description, Type.PROVISION, getEffect(elements[4]), getScore(elements[5]), parseValue(elements[6]));
		}
		else if (type==Type.WEAPON || type==Type.ARMOR || type==Type.SHIELD)
		{
			if (elements[4].equalsIgnoreCase("NONE"))
			{
				return new Item(itemId, name, description, type, DefaultEquippable.INSTANCE);
			}
			else
			{
				return createMagicEquippable(itemId, name, description, type, getScore(elements[5]), parseValue(elements[6]));
			}
		}
		else {
			return new Item(itemId, name, description, type);
		}
	}

	public static Item createMagicEquippable(int itemId, String name, String description, Type type, AffectedScoreRetriever score, int value)
	{
		return new Item(
				itemId,
				name,
				description,
				type,
				new MagicEquippable(
						value,
						score
				)
		);

	}

	public static int parseValue(String value)
	{
		if (value.equalsIgnoreCase("MAX"))
		{
			return Integer.MAX_VALUE;
		}
		else
		{
			return Integer.parseInt(value);
		}
	}

	public static Item createConsumable(int itemId, String name, String description, Type type, ChangeScoreEffect effect, AffectedScoreRetriever score, int value)
	{
		return new Item(
				itemId,
				name,
				description,
				type,
				new DefaultConsumable(
						value,
						score,
						effect
				)
		);
	}

	public static ChangeScoreEffect getEffect(String effect)
	{
		if (effect.equals("RAISE"))
		{
			return GameRules.RAISE;
		}
		else if (effect.equals("HARM"))
		{
			return GameRules.HARM;
		}
		else if (effect.equals("RESTORE"))
		{
			return GameRules.RESTORE;
		}
		else {
			throw new IllegalArgumentException("Unknown effect : "+effect);
		}
	}

	public static AffectedScoreRetriever getScore(String score)
	{
		if (score.equals("LUCK"))
		{
			return GameRules.RETRIEVE_LUCK;
		}
		else if (score.equals("SKILL"))
		{
			return GameRules.RETRIEVE_SKILL;
		}
		else if (score.equals("STAMINA"))
		{
			return GameRules.RETRIEVE_STAMINA;
		}
		else {
			throw new IllegalArgumentException("Unknown score : "+score);
		}
	}
}
