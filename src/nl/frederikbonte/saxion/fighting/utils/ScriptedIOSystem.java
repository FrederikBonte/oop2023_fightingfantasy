package nl.frederikbonte.saxion.fighting.utils;

import java.io.*;

public class ScriptedIOSystem extends AbstractIOSystem implements Closeable
{
	private final BufferedReader reader;

	private ScriptedIOSystem(BufferedReader reader)
	{
		this.reader = reader;
	}

	public ScriptedIOSystem(InputStream inputStream)
	{
		this(new BufferedReader(new InputStreamReader(inputStream)));
	}


	public ScriptedIOSystem(String filename) throws FileNotFoundException
	{
		this(new BufferedReader(new FileReader(filename)));
	}

	@Override
	public void close() throws IOException
	{
		this.reader.close();
	}

	@Override
	protected String readText()
	{
		try
		{
			return reader.readLine();
		}
		catch (IOException e)
		{
			System.err.println("Failed to read text input : "+e.getMessage());
			e.printStackTrace();
			return "";
		}
	}

	@Override
	protected int readInteger()
	{
		String number = readText();
		try
		{
			return Integer.parseInt(number);
		}
		catch (NumberFormatException e)
		{
			System.err.println("Failed to parse integer "+number+" : "+e.getMessage());
			e.printStackTrace();
			return -1;
		}
	}

	@Override
	public void print(String text)
	{
		System.out.print(text);
	}

	@Override
	public void println(String text)
	{
		System.out.println(text);
	}

	@Override
	public void pause()
	{
		// No pause for scripted input...
	}
}
