package nl.frederikbonte.saxion.fighting.utils;

public class Score {
    private int initial, current;

    public Score(int value)
    {
        this.initial = value;
        this.current = value;
    }

    public int getValue()
    {
        return this.current;
    }

    public int getMaxValue()
    {
        return this.initial;
    }

    public void decrease(int value)
    {
        this.current -= value;
        if (this.current<0)
        {
            this.current = 0;
        }
    }

    public void increase(int value)
    {
        this.current += value;
        if (current>initial)
        {
            current = initial;
        }
    }

    public void raise(int value)
    {
        initial+=value;
        current = initial;
    }

    public void gainTemporaryImprove(int value)
    {
        initial+=value;
        current+=value;
    }

    /**
     * Better not drop an item that currently grants you improved stamina when you only have 1 left...
     * @param value
     */
    public void loseTemporaryImprove(int value)
    {
        initial-=value;
        current-=value;
    }
}
