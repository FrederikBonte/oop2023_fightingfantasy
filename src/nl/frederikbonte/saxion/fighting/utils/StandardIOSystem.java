package nl.frederikbonte.saxion.fighting.utils;

import java.util.Scanner;

public class StandardIOSystem extends AbstractIOSystem
{
	private final Scanner s = new Scanner(System.in);

	@Override
	public void print(String text)
	{
		System.out.print(text);
	}

	@Override
	public void println(String text)
	{
		System.out.println(text);
	}

	@Override
	public boolean readYesNo(String message)
	{
		return super.readYesNo(message);
	}

	@Override
	public int readInteger()
	{
		return s.nextInt();
	}

	@Override
	public String readText()
	{
		return s.nextLine();
	}

	@Override
	public void pause()
	{
		println("Press enter to continue.");
		s.nextLine();
	}
}
