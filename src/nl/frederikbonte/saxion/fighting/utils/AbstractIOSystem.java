package nl.frederikbonte.saxion.fighting.utils;

import nl.frederikbonte.saxion.fighting.Character;
import nl.frederikbonte.saxion.fighting.items.Item;

import java.util.List;

public abstract class AbstractIOSystem
{
	public void printCharacter(Character character)
	{
		println("\""+character.getName()+"\"");
		println("Skill : "+character.getCurrentSkill()+"/"+character.getInitialSkill());
		println("Stamina : "+character.getCurrentStamina()+"/"+character.getInitialStamina());
		println("Luck : "+character.getCurrentLuck()+"/"+character.getInitialLuck());
		print("Weapon",character.getWeapon());
		print("Armor",character.getArmor());
		print("Shield",character.getShield());
	}

	private void print(String message, Item item)
	{
		print(message);
		print(" : ");
		if (item==null)
		{
			println("none");
		}
		else
		{
			println(item.getName());
		}
	}

	public int printMenu(String... options)
	{
		println("Please select one of the following:");
		for (int i = 0; i < options.length; i++)
		{
			print(i + 1);
			print(" : ");
			println(options[i]);
		}
		println("0 : Quit/Back/Cancel");
		return readInteger("Enter your selection", options.length);
	}

	public <T> T printMenu(List<T> options)
	{
		String[] items = new String[options.size()];
		for (int i = 0; i < options.size(); i++)
		{
			items[i] = options.get(i).toString();
		}
		int index = printMenu(items);
		if (index==0)
		{
			return null;
		}
		else {
			return options.get(index-1);
		}
	}

	public void print(int value)
	{
		print(""+value);
	}
	public void print(boolean value)
	{
		print(""+value);
	}
	public void print(boolean value, String trueValue, String falseValue)
	{
		print(value?trueValue:falseValue);
	}
	public void print(double value)
	{
		print(String.format("%3.3f", value));
	}

	public int readInteger(String message, int max)
	{
		int answer = -1;
		do
		{
			print(message);
			print(" : ");
			answer = readInteger();
		} while (answer<0 || answer>max);
		return answer;
	}

	public String readText(String message)
	{
		print(message);
		print(" : ");
		return readText();
	}

	public boolean readYesNo(String message)
	{
		return readYesNo(message, true);
	}

	public boolean readYesNo(String message, boolean defaultYes)
	{
		println(message);
		print("[Y]es/[N]o");
		print(defaultYes, "(default is yes)", "(default is no)");
		print(" : ");
		return readYesNo(defaultYes);
	}

	public boolean readYesNo(boolean defaultYes)
	{
		String answer = readText().trim().toLowerCase();
		if (defaultYes)
		{
			// Anything but NO defaults to yes.
			return !answer.equals("no") && !answer.equals("n");
		}
		else
		{
			// Anything but YES defaults to no.
			return answer.equals("yes") || answer.equals("y");
		}
	}

	public void println()
	{
		println("");
	}

	public abstract void pause();

	protected abstract String readText();
	protected abstract  int readInteger();
	public abstract void print(String text);
	public abstract void println(String text);
}
