package nl.frederikbonte.saxion.fighting;

import nl.frederikbonte.saxion.fighting.utils.AbstractIOSystem;
import nl.frederikbonte.saxion.fighting.utils.RandomSource;

public class Battle
{
	private final AbstractIOSystem ioSystem;
	private final Character player, monster;

	private final boolean escape;

	public Battle(AbstractIOSystem ioSystem, Character player, Character monster, boolean escape)
	{
		this.ioSystem = ioSystem;
		this.player = player;
		this.monster = monster;
		this.escape = escape;
	}

	public Battle(AbstractIOSystem ioSystem, Character player, Character monster)
	{
		this(ioSystem, player, monster, false);
	}

	public boolean isOver()
	{
		return player.isDead() || monster.isDead();
	}

	public boolean canEscape()
	{
		return escape;
	}

	public void doAttackRound()
	{
		int monsterRoll = RandomSource.roll(2, monster.getCurrentSkill());
		int playerRoll = RandomSource.roll(2, player.getCurrentSkill());
		if (monsterRoll==playerRoll)
		{
			ioSystem.println("Your attack is blocked.");
		}
		else if (monsterRoll>playerRoll)
		{
			damagePlayer("The monster hits you. Do you wish to use luck to reduce the damage?");
		}
		else
		{
			damageMonster("You hit the monster. Do you wish to use luck to increase the damage?");
		}
	}

	private void damageMonster(String message)
	{
		boolean useLuck = false;
		if (monster.getCurrentStamina()>2)
		{
			useLuck = ioSystem.readYesNo(message);
		}
		if (useLuck && GameRules.useLuck(player))
		{
			ioSystem.println("You are lucky and do extra damage.");
			GameRules.takeDamage(monster, 4);
		}
		else
		{
			ioSystem.println("You damage the monster.");
			GameRules.takeDamage(monster, 2);
		}
	}

	private void damagePlayer(String message)
	{
		boolean useLuck = ioSystem.readYesNo(message);
		if (useLuck && GameRules.useLuck(player))
		{
			ioSystem.println("You are lucky and only take one point of damage.");
			GameRules.takeDamage(player, 1);
		}
		else
		{
			ioSystem.println("You take two points of damage.");
			GameRules.takeDamage(player, 2);
		}
	}

	public void escape()
	{
		damagePlayer("Because you run away, the monster will deal two damage. Do you wish to use luck to try and reduce that?");
	}

}
