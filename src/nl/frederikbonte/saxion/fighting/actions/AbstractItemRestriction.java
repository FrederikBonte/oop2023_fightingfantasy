package nl.frederikbonte.saxion.fighting.actions;

import nl.frederikbonte.saxion.fighting.Adventure;
import nl.frederikbonte.saxion.fighting.items.Item;

public abstract class AbstractItemRestriction implements EffectRestriction
{
    private final Adventure adventure;
    private final int itemId;

    private Item item = null;

    public AbstractItemRestriction(Adventure adventure, int itemId)
    {
        this.adventure = adventure;
        this.itemId = itemId;
    }

    public AbstractItemRestriction(Item item)
    {
        this(null, -1);
        this.item = item;
    }

    public Item getItem()
    {
        if (item==null)
        {
            item = adventure.getItem(itemId);
        }
        return item;
    }
}
