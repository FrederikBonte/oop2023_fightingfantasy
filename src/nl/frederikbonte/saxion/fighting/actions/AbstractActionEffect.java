package nl.frederikbonte.saxion.fighting.actions;

public abstract class AbstractActionEffect implements ActionEffect
{
    private final EffectRestriction restriction;

    protected AbstractActionEffect(EffectRestriction restriction)
    {
        this.restriction = restriction;
    }

    @Override
    public EffectRestriction getRestriction()
    {
        return restriction;
    }

}
