package nl.frederikbonte.saxion.fighting.actions;

import nl.frederikbonte.saxion.fighting.Character;

public interface ActionEffect
{
	EffectRestriction getRestriction();

	void execute(Character character);
}
