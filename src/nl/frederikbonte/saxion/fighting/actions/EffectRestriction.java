package nl.frederikbonte.saxion.fighting.actions;

import nl.frederikbonte.saxion.fighting.Character;

public interface EffectRestriction
{
    static final EffectRestriction NO_RESTRICTION = new EffectRestriction()
    {
        @Override
        public boolean canTrigger(Character character)
        {
            return true;
        }
    };

    boolean canTrigger(Character character);
}
