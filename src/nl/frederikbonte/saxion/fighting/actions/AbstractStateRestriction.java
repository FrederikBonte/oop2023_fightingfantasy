package nl.frederikbonte.saxion.fighting.actions;

import nl.frederikbonte.saxion.fighting.StoryElement;
import nl.frederikbonte.saxion.fighting.items.Item;

public abstract class AbstractStateRestriction implements EffectRestriction
{
    private final StoryElement element;
    private final Item item;

    public AbstractStateRestriction(StoryElement element, Item item)
    {
        this.element = element;
        this.item = item;
    }
}
