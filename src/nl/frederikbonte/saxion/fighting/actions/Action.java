package nl.frederikbonte.saxion.fighting.actions;

public class Action
{
	private String text;
	private ActionEffect effect;

	public Action(String text, ActionEffect effect)
	{
		this.text = text;
		this.effect = effect;
	}

	@Override
	public String toString()
	{
		return text;
	}

	public ActionEffect getEffect()
	{
		return this.effect;
	}
}
