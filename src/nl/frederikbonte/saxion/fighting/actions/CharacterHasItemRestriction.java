package nl.frederikbonte.saxion.fighting.actions;

import nl.frederikbonte.saxion.fighting.Adventure;
import nl.frederikbonte.saxion.fighting.Character;

public class CharacterHasItemRestriction extends AbstractItemRestriction
{
    public CharacterHasItemRestriction(Adventure adventure, int itemId)
    {
        super(adventure, itemId);
    }

    @Override
    public boolean canTrigger(Character character)
    {
        return character.hasItem(getItem());
    }
}
