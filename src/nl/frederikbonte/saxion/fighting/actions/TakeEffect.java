package nl.frederikbonte.saxion.fighting.actions;

import nl.frederikbonte.saxion.fighting.Character;
import nl.frederikbonte.saxion.fighting.GameRules;
import nl.frederikbonte.saxion.fighting.StoryElement;
import nl.frederikbonte.saxion.fighting.items.Item;
import nl.frederikbonte.saxion.fighting.items.Type;

public class TakeEffect extends AbstractActionEffect
{
	private final StoryElement element;
	private final int itemId;

	public TakeEffect(StoryElement element, int itemId, EffectRestriction restriction)
	{
		super(restriction);
		this.element = element;
		this.itemId = itemId;
	}

	public TakeEffect(StoryElement element, int itemId)
	{
		this(element, itemId, new EquippableItemRestriction(element.getItem(itemId)));
	}

	public StoryElement getElement()
	{
		return element;
	}

	public int getItemId()
	{
		return itemId;
	}

	@Override
	public void execute(Character character)
	{
		// Take the item from the room.
		Item item = getElement().remove(getItemId());
		if (item.getEquippable().isEquippable() && item.getEquippable().canEquip(character))
		{
			// Check if the character already holds a similar item. (Can't hold two swords.)
			Item similarItem = getSimilarItem(character, item.getType());
			// If there is a similar item.
			if (similarItem!=null)
			{
				// Unequip the item.
				GameRules.unequip(character, similarItem);
				// Place the item in the room.
				getElement().add(similarItem);
			}
			// Equip the new item.
			GameRules.equip(character, item);
		}
		else
		{
			// @TODO: Simply carry the item with you...
			character.add(item);
		}
	}

	private Item getSimilarItem(Character character, Type type)
	{
		if (type == Type.WEAPON)
		{
			return character.getWeapon();
		}
		else if (type == Type.ARMOR)
		{
			return character.getArmor();
		}
		else if (type == Type.SHIELD)
		{
			return character.getShield();
		}
		else
		{
			return null;
		}
	}
}
