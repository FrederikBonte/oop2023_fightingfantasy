package nl.frederikbonte.saxion.fighting.actions;

import nl.frederikbonte.saxion.fighting.Character;
import nl.frederikbonte.saxion.fighting.items.Item;

public class EquippableItemRestriction extends AbstractItemRestriction
{
    public EquippableItemRestriction(Item item)
    {
        super(item);
    }

    @Override
    public boolean canTrigger(Character character)
    {
        return (getItem().getEquippable().isEquippable() && getItem().getEquippable().canEquip(character)) || !getItem().getEquippable().isEquippable();
    }
}