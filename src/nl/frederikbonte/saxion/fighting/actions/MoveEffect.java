package nl.frederikbonte.saxion.fighting.actions;

import nl.frederikbonte.saxion.fighting.Adventure;
import nl.frederikbonte.saxion.fighting.Character;
import nl.frederikbonte.saxion.fighting.GameRules;

public class MoveEffect extends AbstractActionEffect
{
	private final Adventure adventure;
	private final int targetId;

	public MoveEffect(Adventure adventure, int targetId, EffectRestriction restriction)
	{
		super(restriction);
		this.adventure = adventure;
		this.targetId = targetId;
	}

	@Override
	public void execute(Character character)
	{
		GameRules.move(character, adventure.getElement(targetId));
	}
}
