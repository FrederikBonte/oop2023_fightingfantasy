package nl.frederikbonte.saxion.fighting.items;

public enum Type
{
	WEAPON,
	ARMOR,
	SHIELD,
	POTION,
	PROVISION,
	LIGHTSOURCE,
	TREASURE,
	KEY
}
