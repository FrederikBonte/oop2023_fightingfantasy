package nl.frederikbonte.saxion.fighting.items;

import nl.frederikbonte.saxion.fighting.Character;

public class DefaultEquippable implements Equippable
{
	public static final DefaultEquippable INSTANCE = new DefaultEquippable();
	private DefaultEquippable() {}

	@Override
	public boolean isEquippable()
	{
		return true;
	}

	@Override
	public void equip(Character character)
	{
		// No effect.
	}

	@Override
	public void unequip(Character character)
	{
		// No effect.
	}

	@Override
	public boolean canEquip(Character character)
	{
		return true;
	}
}
