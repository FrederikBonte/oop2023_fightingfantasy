package nl.frederikbonte.saxion.fighting.items;

import nl.frederikbonte.saxion.fighting.Character;
import nl.frederikbonte.saxion.fighting.utils.Score;

/**
 * When equipping an item with this effect an attribute is temporarily changed.
 * Normally a sword for instance will raise the characters skill lever.
 * A cursed item might lower a certain score, but then cannot be unequipped until a certain condition comes up?
 */
public class MagicEquippable implements Equippable
{
	private final int value;

	private final AffectedScoreRetriever scoreRetriever;

	public MagicEquippable(int value, AffectedScoreRetriever scoreRetriever)
	{
		this.value = value;
		this.scoreRetriever = scoreRetriever;
	}

	@Override
	public boolean isEquippable()
	{
		return true;
	}

	@Override
	public void equip(Character character)
	{
		Score score = scoreRetriever.getScore(character);
		score.gainTemporaryImprove(value);
	}

	@Override
	public void unequip(Character character)
	{
		Score score = scoreRetriever.getScore(character);
		score.loseTemporaryImprove(value);
	}

	@Override
	public boolean canEquip(Character character)
	{
		return true;
	}
}
