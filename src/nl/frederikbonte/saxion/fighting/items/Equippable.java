package nl.frederikbonte.saxion.fighting.items;

import nl.frederikbonte.saxion.fighting.Character;

public interface Equippable
{
	/**
	 * Returns <i>true</i> when this is an equippable {@link Item}.
	 * @return <i>true</i> when this is an equippable {@link Item}.
	 */
	boolean isEquippable();
	void equip(Character character);
	void unequip(Character character);

	/**
	 * Returns <i>true</i> when the provided {@link Character} is allowed to equip this {@link Item}.
	 * For instance an {@link Item} may require the {@link Character} to be a cleric, or a {@link Character}
	 * is already wielding an item with the similar type.
	 * @param character The {@link Character} that wishes to equip this {@link Item}.
	 * @return <i>true</i> when the provided {@link Character} is allowed to equip this item.
	 */
	boolean canEquip(Character character);
}
