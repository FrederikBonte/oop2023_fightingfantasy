package nl.frederikbonte.saxion.fighting.items;

import nl.frederikbonte.saxion.fighting.Character;

public interface Consumable
{
	boolean isConsumable();
	boolean canConsume(Character character);
	void consume(Character character);
}
