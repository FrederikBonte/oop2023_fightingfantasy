package nl.frederikbonte.saxion.fighting.items;

import nl.frederikbonte.saxion.fighting.utils.Score;


@FunctionalInterface
public interface ChangeScoreEffect
{
	public void change(Score score, int value);
}
