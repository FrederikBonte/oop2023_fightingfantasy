package nl.frederikbonte.saxion.fighting.items;

public class Item
{
	private final int id;
	private final String name, description;
	private final Type type;
	private Consumable consumable;
	private Equippable equippable;

	Item(int id, String name, String description, Type type, Consumable consumable, Equippable equippable)
	{
		this.id	= id;
		this.name = name;
		this.description = description;
		this.type = type;
		this.consumable = consumable;
		this.equippable = equippable;
	}

	/**
	 * Instantiate a consumable item. This item will by default not be equippable.
	 * @param name the name of the item.
	 * @param description a short description of the item.
	 * @param type the {@link Type} of item.
	 * @param consumable the {@link Consumable} effect that triggers when consuming the item.
	 */
	public Item(int id, String name, String description, Type type, Consumable consumable)
	{
		this(id, name, description, type, consumable, NotEquippable.INSTANCE);
	}

	/**
	 * Instantiate an equippable item. This item will by default be unequippable.
	 * @param name the name of the item.
	 * @param description a short description of the item.
	 * @param type the {@link Type} of item.
	 * @param equippable the {@link Equippable} effect that triggers when a {@link Character} wields the item.
	 */
	public Item(int id, String name, String description, Type type, Equippable equippable)
	{
		this(id, name, description, type, NotConsumable.INSTANCE, equippable);
	}

	/**
	 * Instantiate an item that can neither be consumed nor equipped.
	 * For instance a {@link Type#TREASURE} like a gem, a key, stuff like that.
	 * Having such an item in your inventory may unlock actions that you normally wouldn't have.
	 * @param name the name of the item.
	 * @param description a short description of the item.
	 * @param type the {@link Type} of item.
	 */
	public Item(int id, String name, String description, Type type)
	{
		this(id, name, description, type, NotConsumable.INSTANCE, NotEquippable.INSTANCE);
	}

	protected void setConsumable(Consumable consumable)
	{
		this.consumable = consumable;
	}

	protected void setEquippable(Equippable equippable)
	{
		this.equippable = equippable;
	}

	public int getId()
	{
		return id;
	}

	public String getName()
	{
		return name;
	}

	public String getDescription()
	{
		return description;
	}

	public Type getType()
	{
		return type;
	}

	public Consumable getConsumable()
	{
		return consumable;
	}

	public Equippable getEquippable()
	{
		return equippable;
	}

}
