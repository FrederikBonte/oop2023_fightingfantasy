package nl.frederikbonte.saxion.fighting.items;

import nl.frederikbonte.saxion.fighting.Character;

public class NotConsumable implements Consumable
{
	public static final NotConsumable INSTANCE = new NotConsumable();

	private NotConsumable()
	{
	}

	@Override
	public boolean isConsumable()
	{
		return false;
	}

	public boolean canConsume(Character character)
	{
		return false;
	}

	@Override
	public void consume(Character character)
	{
		throw new UnsupportedOperationException("Cannot consume unconsummable item.");
	}
}
