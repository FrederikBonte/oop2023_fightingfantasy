package nl.frederikbonte.saxion.fighting.items;

import nl.frederikbonte.saxion.fighting.Character;
import nl.frederikbonte.saxion.fighting.utils.Score;

@FunctionalInterface
public interface AffectedScoreRetriever
{
	Score getScore(Character character);
}
