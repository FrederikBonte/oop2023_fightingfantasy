package nl.frederikbonte.saxion.fighting.items;

import nl.frederikbonte.saxion.fighting.utils.Score;
import nl.frederikbonte.saxion.fighting.Character;

public class DefaultConsumable implements Consumable
{
	private final int value;
	private final AffectedScoreRetriever scoreRetriever;
	private final ChangeScoreEffect scoreEffect;

	public DefaultConsumable(int value, AffectedScoreRetriever retriever, ChangeScoreEffect scoreEffect)
	{
		this.value = value;
		this.scoreRetriever = retriever;
		this.scoreEffect = scoreEffect;
	}

	@Override
	public boolean isConsumable()
	{
		return true;
	}

	public final boolean canConsume(Character character)
	{
		return true;
	}

	@Override
	public void consume(Character character)
	{
		Score score = scoreRetriever.getScore(character);
		scoreEffect.change(score, value);
	}
}
