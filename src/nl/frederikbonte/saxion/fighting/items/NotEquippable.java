package nl.frederikbonte.saxion.fighting.items;

import nl.frederikbonte.saxion.fighting.Character;

public class NotEquippable implements Equippable
{
	public static final NotEquippable INSTANCE = new NotEquippable();

	private NotEquippable() {}
	@Override
	public boolean isEquippable()
	{
		return false;
	}

	@Override
	public void equip(Character character)
	{
		throw new UnsupportedOperationException("Cannot equip unequippable item.");
	}

	@Override
	public void unequip(Character character)
	{
		throw new UnsupportedOperationException("Cannot unequip unequippable item.");
	}

	@Override
	public boolean canEquip(Character character)
	{
		return false;
	}
}
