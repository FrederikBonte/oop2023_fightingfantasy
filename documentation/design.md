# [Fighting Fantasy](https://www.fightingfantasy.com/)
## [The Warlock of Firetop Mountain](https://shop.scholastic.co.uk/products/113603)
### By [Steve Jackson](http://www.sjgames.com/) & Ian Livingstone

# Primary requirements

* Player must be able to move from location to location.
* Player must be able to enter combat.
* Not all combats can be escaped.
* Rooms also contain traps.
* Other actions are possible.
  * Talk to person.
  * Yell
  * Attack
  * Leave
* Pick up item.
* Replace current equipment
* User resources in inventory.
  * Consume provisions.
  * Drink potion.
  * etc.

## Other activities

* Enchant existing item/Use scroll to change
  * Effect of magical weapon/shield/armor
  * Change situation in location.
  * Dispell blocking enchantment.
  * etc.

# Characters (and monsters)

A character has three ability score, each of these has an initial value and a current value. 
For instance the characters health is indicated through stamina. 
During battle a character may lose stamina points. When this score reaches 0 the character
is dead. Potions may be consumed to restore the score to its initial value.

Battles are handled by the Battle class. It checks if the battle is still going,
and each turn is dealt between both combatants. The IOSystem is used to ask if the
player wishes to use luck to influence the damage given and/or taken.

The rules of the game and all their consequences will be implemented in the GameRules.


```mermaid
classDiagram
    class Character {
        -name : String
        
        +Character(name : String, skill : int, stamina : int, luck : int)
        
        +getName() String
        getLuck() Score
        getStamina() Score
        getSkill() Score
        +getCurrentStamina() int
        +getCurrentSkill() int
        +getCurrentLuck() int
        +getInitialStamina() int
        +getInitialSkill() int
        +getInitialLuck() int
        +isAlive() boolean
        +isDead() boolean
        +getWeapon() Item
        +getShield() Item
        +getArmor() Item
        +equip(item : Item) boolean
        +unequip(item : Item) boolean
    }

    Character --> Score : skill
    Character --> Score : stamina
    Character --> Score : luck
    
    class Score {
        -initial : int
        -current : int
        
        +Score(initial : int)
        +getValue() int
        +getmaxValue() int
        
        +decrease(value : int) void
        +increase(value : int) void
        +raise(value : int) void
        +gainTemporaryImprove(value : int) void
        +loseTemporaryImprove(value : int) void
    }
    
    Character --> Item : weapon
    Character --> Item : shield
    Character --> Item : armor
    Character o--> Item : inventory

    Character -- GameRules
    Battle -- GameRules
    Item -- GameRules

    Character -- Battle : player
    Character -- Battle : monster
    IOSystem -- Battle : io

    class Item {
        +getName() String
        +getDescription() String
        +getType() Type
    }
    
    class Battle {
        +Battle(io : IOSystem, player : Character, monster : Character)
        +isOver() boolean
        +doAttackRound() void
    }
    
    class GameRules {
        +takeDamage(character : Character, damage : int)$ void
        +useLuck(character : Character)$ boolean
        +consume(character : Character, item : Item)$ void
        +equip(character : Character)$ void
        +unequip(character : Character)$ void
    }

```

# Items
The book specifies a specific set of items:
* Weapon (Sword, Knife, Bow, etc.)
* Shield
* Armor
* "A lantern to light your way." - page 16
* Provisions
* Potions
  * Potion of Strength
  * Potion of Skill
  * Potion of Fortune
* Treasure
* Keys
* Traps?

The character has a backpack (inventory) but weapons, armor and shields can only
be replaced. The game will leave the dropped item in the current location.

"Not all rooms contain treasure; many merely contain traps and monsters... 
Several keys are to be found... (only) with the correct keys will you ... get to
(the) treasure."

```mermaid
classDiagram

    Consumable <|-- NotConsumable
    Equippable <|-- NotEquippable
    Consumable <|-- DefaultConsumable

    AffectedScoreRetriever <|-- StaminaRetriever
    AffectedScoreRetriever <|-- SkillRetriever
    AffectedScoreRetriever <|-- LuckRetriever

    ChangeScoreEffect <|-- HarmEffect
    ChangeScoreEffect <|-- RestoreEffect
    ChangeScoreEffect <|-- RaiseEffect

    DefaultConsumable --> AffectedScoreRetriever
    DefaultConsumable --> ChangeScoreEffect

    Item *--> Consumable
    Item *--> Equippable
    
    Equippable <|-- DefaultEquippable
    Equippable <|-- MagicEquippable

    <<interface>> Consumable
    <<interface>> Equippable

    class Item {
        -name : String
        -description : String
        -type : Type
        -equippable: Equippable
        -consumable: Consumable
        
        +Item(name : String, description : String, type : Type)
        
        +getName() String
        +getDescription() String
        +getType() Type
        +getConsumable() Consumable
        +getEquippable() Equippable
    }
 
    class Consumable {
        +isConsumable() boolean
        +canConsume(character : Character) boolean
        +consume(character : Character) void
    }
    
    class Equippable {
        +isEquippable() boolean
        +canEquip(character : Character) boolean
        +equip(character : Character) void
        +unequip(character : Character) void
    }

    class DefaultConsumable {
        -value : int
        -retriever : AffectedScoreRetriever
        -effect : ChangeScoreEffect
        +DefaultConsumable()
    }
    
    class AffectedScoreRetriever {
        +getScore(Character character) Score
    }
    
    
    class ChangeScoreEffect {
        +change(score : Score, value : int)
    }
    
```