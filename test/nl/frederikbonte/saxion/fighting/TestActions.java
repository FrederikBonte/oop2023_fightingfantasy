package nl.frederikbonte.saxion.fighting;

import nl.frederikbonte.saxion.fighting.actions.Action;
import nl.frederikbonte.saxion.fighting.actions.ActionEffect;
import nl.frederikbonte.saxion.fighting.actions.EffectRestriction;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class TestActions
{
	public static final ActionEffect NO_EFFECT = new ActionEffect()
	{
		@Override
		public EffectRestriction getRestriction()
		{
			return GameRules.NO_RESTRICTION;
		}

		@Override
		public void execute(Character character)
		{

		}
	};
	@Test
	public void testBasicAction()
	{
		Action action = new Action("Do stuff", NO_EFFECT);
		assertEquals("Do stuff", action.toString());
		assertSame(NO_EFFECT, action.getEffect());
		assertSame(GameRules.NO_RESTRICTION, action.getEffect().getRestriction());
		assertTrue(action.getEffect().getRestriction().canTrigger(null));
		// Since the action has no effect, you can simply call the execute function.
		action.getEffect().execute(null);
	}

	@Test
	public void testStoryElement()
	{
		StoryElement element = new StoryElement(0, "Story text", "myimage.png");
		assertEquals(0, element.getId());
		assertEquals("Story text", element.getText());
		assertEquals("myimage.png", element.getImage());
		assertEquals(0, element.items.size());
		assertTrue(element.items.isEmpty());
		assertEquals(0, element.actions.size());
		assertTrue(element.actions.isEmpty());
	}

	@Test
	public void testMovement()
	{
		StoryElement element1 = new StoryElement(1, "Room 1");
		StoryElement element2 = new StoryElement(1, "Room 1");
	}

}
