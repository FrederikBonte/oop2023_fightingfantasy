package nl.frederikbonte.saxion.fighting;

import nl.frederikbonte.saxion.fighting.utils.ScriptedIOSystem;
import org.junit.jupiter.api.*;

import java.io.ByteArrayInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import static org.junit.jupiter.api.Assertions.*;

public class TestScriptedIOSystem
{
	private static final String DUMMY_SCRIPT = "Mark\n" +
			"y\n" +
			"yes\n" +
			"\n" +
			"n\n" +
			"no\n" +
			"\n" +
			"1\n" +
			"-15\n" +
			"fnork\n" +
			"2";

	private static final String SA_SCRIPT = "1\n2\n1\n1\n2\n2\n0\n0\n0\n0\n";

	@Test
	public void testScriptedInput()
	{
		ScriptedIOSystem ioSystem = new ScriptedIOSystem(new ByteArrayInputStream(DUMMY_SCRIPT.getBytes()));
		String input = ioSystem.readText("Please type your name");
		assertEquals("Mark", input);
		assertTrue(ioSystem.readYesNo("Wanna?"));
		assertTrue(ioSystem.readYesNo("Wanna?"));
		assertTrue(ioSystem.readYesNo("Wanna?"));
		assertFalse(ioSystem.readYesNo("Wanna?", false));
		assertFalse(ioSystem.readYesNo("Wanna?", false));
		assertFalse(ioSystem.readYesNo("Wanna?", false));
		assertEquals(1, ioSystem.readInteger("Give a number", 10));
		assertEquals(2, ioSystem.readInteger("Give a number", 10));
	}

	@Test
	public void testSimpleAdventure()
	{
		ScriptedIOSystem ioSystem = new ScriptedIOSystem(new ByteArrayInputStream(SA_SCRIPT.getBytes()));
		Character kronk = new Character("Kronk", 8, 17, 9);
		Adventure adventure;
		try
		{
			adventure = new Adventure("/adventure_v1.csv");
		}
		catch (IOException e)
		{
			throw new RuntimeException("Just in case the reading fails...");
		}
		adventure.startAdventure(kronk);
		assertEquals(1, kronk.getStoryElement().getId());
		assertEquals(adventure.getItem(1), kronk.getWeapon());
		assertEquals(adventure.getItem(2), kronk.getArmor());
		assertEquals(adventure.getItem(3), kronk.getShield());
		assertFalse(kronk.hasItem(adventure.getItem(4)));
		assertFalse(kronk.hasItem(adventure.getItem(5)));
		assertFalse(kronk.hasItem(adventure.getItem(6)));

		kronk.getStoryElement().showMenu(ioSystem, kronk);
		assertEquals(2, kronk.getStoryElement().getId());

		kronk.getStoryElement().showMenu(ioSystem, kronk);
		assertTrue(kronk.hasItem(adventure.getItem(4)));

		kronk.getStoryElement().showMenu(ioSystem, kronk);
		assertEquals(3, kronk.getStoryElement().getId());

		kronk.getStoryElement().showMenu(ioSystem, kronk);
		assertEquals(4, kronk.getStoryElement().getId());

		kronk.getStoryElement().showMenu(ioSystem, kronk);
		assertTrue(kronk.hasItem(adventure.getItem(5)));

		kronk.getStoryElement().showMenu(ioSystem, kronk);
		assertEquals(adventure.getItem(6), kronk.getWeapon());
	}
}
