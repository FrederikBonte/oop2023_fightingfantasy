package nl.frederikbonte.saxion.fighting;

import nl.frederikbonte.saxion.fighting.items.DefaultConsumable;
import nl.frederikbonte.saxion.fighting.items.Item;
import nl.frederikbonte.saxion.fighting.items.MagicEquippable;
import nl.frederikbonte.saxion.fighting.items.Type;
import nl.frederikbonte.saxion.fighting.utils.ItemReader;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.*;

public class TestGameRules
{
	private Character mike;
	private ItemReader reader;

	@BeforeEach
	public void createMike() throws IOException
	{
		mike = new Character("Dead Mike", 5, 15, 10);
		reader = new ItemReader("/items.csv");
		GameRules.equip(mike, reader.items.get(6));
		GameRules.equip(mike, reader.items.get(7));
		GameRules.equip(mike, reader.items.get(8));
		//CharacterIO.printCharacter(mike);
	}

	@Test
	public void testWeapon()
	{
		Throwable exception;

		Item weapon = reader.items.get(6);
		Item magicWeapon = reader.items.get(9);

		assertNotNull(mike.getWeapon());

		// Try to equip the weapon.
		exception = assertThrows(
				UnsupportedOperationException.class, () -> {
					GameRules.equip(mike, magicWeapon);
				}
		);
		assertEquals("Dead Mike already has another weapon equipped.", exception.getMessage());
		GameRules.unequip(mike, weapon);
		// Confirm that mike is not holding a weapon.
		assertNull(mike.getWeapon());

		// Try to unequip the sword again.
		exception = assertThrows(
				UnsupportedOperationException.class, () -> {
					GameRules.unequip(mike, weapon);
				}
		);
		assertEquals("Dead Mike is not currently equipping Sword", exception.getMessage());

		// Check that skill increases when the magic sword is equipped.
		assertEquals(5, mike.getCurrentSkill());
		assertEquals(5, mike.getInitialSkill());
		GameRules.equip(mike, magicWeapon);
		TestItems.assertEquippable(mike.getWeapon(), "Skillful sword", Type.WEAPON, MagicEquippable.class);
		assertEquals(7, mike.getCurrentSkill());
		assertEquals(7, mike.getInitialSkill());

		// Test that skill falls back after dropping the weapon.
		GameRules.unequip(mike, magicWeapon);
		assertNull(mike.getWeapon());
		assertEquals(5, mike.getCurrentSkill());
		assertEquals(5, mike.getInitialSkill());
	}

	@Test
	public void testShield()
	{
		Throwable exception;

		Item shield = reader.items.get(8);
		assertNotNull(mike.getShield());

		// Try to equip the shield again.
		exception = assertThrows(
				UnsupportedOperationException.class, () -> {
					GameRules.equip(mike, shield);
				}
		);
		assertEquals("Dead Mike already has another shield equipped.", exception.getMessage());


		GameRules.unequip(mike, shield);
		// Confirm that mike is not holding a weapon.
		assertNull(mike.getShield());

		// Try to unequip the sword again.
		exception = assertThrows(
				UnsupportedOperationException.class, () -> {
					GameRules.unequip(mike, shield);
				}
		);
		assertEquals("Dead Mike is not currently equipping Shield", exception.getMessage());
	}

	@Test
	public void testArmor()
	{
		Throwable exception;

		Item armor = reader.items.get(7);
		assertNotNull(mike.getArmor());

		// Try to equip the armor again.
		exception = assertThrows(
				UnsupportedOperationException.class, () -> {
					GameRules.equip(mike, armor);
				}
		);
		assertEquals("Dead Mike already has another armor equipped.", exception.getMessage());

		GameRules.unequip(mike, armor);
		// Confirm that mike is not holding a weapon.
		assertNull(mike.getArmor());

		// Try to unequip the sword again.
		exception = assertThrows(
				UnsupportedOperationException.class, () -> {
					GameRules.unequip(mike, armor);
				}
		);
		assertEquals("Dead Mike is not currently equipping Armor", exception.getMessage());
	}

	@Test
	public void testSkillPotion()
	{
		Item harmSkillPotion = new Item(0,
			"Harmful", null, Type.POTION,
				new DefaultConsumable(
						100,
						GameRules.RETRIEVE_SKILL,
						GameRules.HARM
				)
		);
		Item potion = reader.items.get(2);

		assertEquals(5, mike.getCurrentSkill());
		assertEquals(5, mike.getInitialSkill());
		GameRules.consume(mike, harmSkillPotion);
		assertEquals(0, mike.getCurrentSkill());
		assertEquals(5, mike.getInitialSkill());
		assertEquals(Type.POTION, potion.getType());
		assertEquals("Potion of Skill", potion.getName());
		GameRules.consume(mike, potion);
		assertEquals(5, mike.getInitialSkill());
		assertEquals(5, mike.getCurrentSkill());
	}


	@Test
	public void testStaminaPotion()
	{
		Item harmSkillPotion = new Item(0,
				"Harmful", null, Type.POTION,
				new DefaultConsumable(
						100,
						GameRules.RETRIEVE_STAMINA,
						GameRules.HARM
				)
		);
		Item potion = reader.items.get(3);

		assertEquals(15, mike.getCurrentStamina());
		assertEquals(15, mike.getInitialStamina());
		GameRules.consume(mike, harmSkillPotion);
		assertEquals(0, mike.getCurrentStamina());
		assertEquals(15, mike.getInitialStamina());
		assertEquals(Type.POTION, potion.getType());
		assertEquals("Potion of Strength", potion.getName());
		GameRules.consume(mike, potion);
		assertEquals(15, mike.getInitialStamina());
		assertEquals(15, mike.getCurrentStamina());
	}


	@Test
	public void testLuckPotion()
	{
		Item harmSkillPotion = new Item(0,
				"Harmful", null, Type.POTION,
				new DefaultConsumable(
						100,
						GameRules.RETRIEVE_LUCK,
						GameRules.HARM
				)
		);
		Item potion = reader.items.get(4);

		assertEquals(10, mike.getCurrentLuck());
		assertEquals(10, mike.getInitialLuck());
		GameRules.consume(mike, harmSkillPotion);
		assertEquals(0, mike.getCurrentLuck());
		assertEquals(10, mike.getInitialLuck());
		assertEquals(Type.POTION, potion.getType());
		assertEquals("Potion of Fortune", potion.getName());
		GameRules.consume(mike, potion);
		assertEquals(11, mike.getInitialLuck());
		assertEquals(11, mike.getCurrentLuck());
	}

	@Test
	public void testUnequippableError1()
	{
		Item item = reader.items.get(0);
		Throwable exception = assertThrows(
				UnsupportedOperationException.class, () -> {
					GameRules.equip(mike, item);
				}
		);

		assertEquals("Cannot equip Cotion of Ponfusion", exception.getMessage());
	}

	@Test
	public void testUnequippableError2()
	{
		Item item = reader.items.get(0);
		Throwable exception = assertThrows(
				UnsupportedOperationException.class, () -> {
					GameRules.unequip(mike, item);
				}
		);

		assertEquals("Cannot unequip Cotion of Ponfusion", exception.getMessage());
	}

	@Test
	public void testUnconsumableError()
	{
		Item item = reader.items.get(0);
		Throwable exception = assertThrows(
				UnsupportedOperationException.class, () -> {
					GameRules.consume(mike, item);
				}
		);

		assertEquals("Cannot consume Cotion of Ponfusion", exception.getMessage());
	}

	@Test
	public void testUnluckyDucky()
	{
		Character ducky = new Character("Unlucky Ducky",1,1,1);
		assertEquals(1, ducky.getCurrentLuck());
		assertEquals(1, ducky.getInitialLuck());
		GameRules.useLuck(ducky);
		assertEquals(0, ducky.getCurrentLuck());
		assertEquals(1, ducky.getInitialLuck());
		assertFalse(GameRules.useLuck(ducky));
		assertEquals(0, ducky.getCurrentLuck());
		assertEquals(1, ducky.getInitialLuck());
		GameRules.consume(ducky, reader.items.get(4));
		assertEquals(2, ducky.getCurrentLuck());
		assertEquals(2, ducky.getInitialLuck());
	}
}
