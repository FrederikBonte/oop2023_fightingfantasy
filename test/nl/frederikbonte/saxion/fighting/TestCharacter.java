package nl.frederikbonte.saxion.fighting;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class TestCharacter {
    private Character mike;

    @BeforeEach
    public void createMike()
    {
        mike = new Character("Dead Mike", 1, 4, 5);
    }

    @Test
    public void testBasicCharacter()
    {
        assertEquals("Dead Mike", mike.getName());
        assertEquals(1, mike.getCurrentSkill());
        assertEquals(1, mike.getInitialSkill());
        assertEquals(4, mike.getCurrentStamina());
        assertEquals(4, mike.getInitialStamina());
        assertEquals(5, mike.getCurrentLuck());
        assertEquals(5, mike.getInitialLuck());
        assertTrue(mike.isAlive());
        assertFalse(mike.isDead());
    }

    public void testLuck()
    {
        GameRules.useLuck(mike);
        assertEquals(4, mike.getLuck());
        assertEquals(5, mike.getInitialLuck());
    }

    public void testDamage()
    {
        GameRules.takeDamage(mike, 4);
        assertTrue(mike.isDead());
        assertFalse(mike.isAlive());
        assertEquals(0, mike.getStamina());
    }

    @Test
    public void testRandomCharacter()
    {
        Character mark = new Character("Random Mark");
        assertEquals("Random Mark", mark.getName());
        assertNull(mark.getWeapon());
        assertNull(mark.getShield());
        assertNull(mark.getArmor());
//        StandardIOSystem ioSystem = new StandardIOSystem();
//        ioSystem.printCharacter(mark);
        assertBetween(7,12, mark.getInitialSkill());
        assertBetween(14,24, mark.getInitialStamina());
        assertBetween(7,12, mark.getInitialLuck());
    }

    private void assertBetween(int lower, int upper, int value)
    {
        assertTrue(value>=lower && value<=upper);
    }
}
