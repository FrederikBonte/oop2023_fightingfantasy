package nl.frederikbonte.saxion.fighting;

import nl.frederikbonte.saxion.fighting.items.*;
import nl.frederikbonte.saxion.fighting.utils.ItemReader;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.*;

public class TestItems
{
	private Character mike;
	private ItemReader reader;

	@BeforeEach
	public void createMike() throws IOException
	{
		mike = new Character("Dead Mike", 5, 15, 10);
		reader = new ItemReader("/items.csv");
		mike.equip(reader.items.get(6));
		mike.equip(reader.items.get(7));
		mike.equip(reader.items.get(8));
	}

	@Test
	public void testBasicEquipment()
	{
		assertEquippable(mike.getWeapon(), "Sword", Type.WEAPON, DefaultEquippable.INSTANCE);
		assertEquippable(mike.getArmor(), "Armor", Type.ARMOR, DefaultEquippable.INSTANCE);
		assertEquippable(mike.getShield(), "Shield", Type.SHIELD, DefaultEquippable.INSTANCE);
	}

	@Test
	public void testEquipUnequipDefault()
	{
		Item potion = new Item(0, "Potion", null, Type.POTION);
		assertFalse(mike.equip(potion));
		assertFalse(mike.unequip(potion));
	}

	public static void assertEquippable(Item item, String name, Type type, DefaultEquippable equippable)
	{
		assertNotNull(item);
		assertEquals(0, item.getId());
		assertEquals(name, item.getName());
		assertEquals(type, item.getType());
		assertEquals(equippable, item.getEquippable());
		assertEquals(NotConsumable.INSTANCE, item.getConsumable());
	}

	public static void assertEquippable(Item item, String name, Type type, Class<? extends Equippable> equippableClass)
	{
		assertNotNull(item);
		assertEquals(name, item.getName());
		assertEquals(type, item.getType());
		assertTrue(item.getEquippable().getClass().isAssignableFrom(equippableClass));
		assertEquals(NotConsumable.INSTANCE, item.getConsumable());
	}

	@Test
	public void testUnequippableError1()
	{
		Item item = reader.items.get(0);
		Throwable exception = assertThrows(
				UnsupportedOperationException.class, () -> {
					item.getEquippable().equip(mike);
				}
		);

		assertEquals("Cannot equip unequippable item.", exception.getMessage());
	}

	@Test
	public void testUnequippableError2()
	{
		Item item = reader.items.get(0);
		Throwable exception = assertThrows(
				UnsupportedOperationException.class, () -> {
					item.getEquippable().unequip(mike);
				}
		);

		assertEquals("Cannot unequip unequippable item.", exception.getMessage());
	}

	@Test
	public void testUnconsumableError()
	{
		Item item = reader.items.get(0);
		Throwable exception = assertThrows(
				UnsupportedOperationException.class, () -> {
					item.getConsumable().consume(mike);
				}
		);

		assertEquals("Cannot consume unconsummable item.", exception.getMessage());
	}

	@Test
	public void testUnequippableUnconsumable()
	{
		Item item = reader.items.get(0);
		assertEquals("Cotion of Ponfusion", item.getName());
		assertEquals("After drinking you get confused", item.getDescription());
		assertUnconsumable(item);
		assertUnequippable(item);
	}

	private void assertUnconsumable(Item item)
	{
		assertSame(NotConsumable.INSTANCE, item.getConsumable());
		assertFalse(item.getConsumable().isConsumable());
		assertFalse(item.getConsumable().canConsume(null));
	}

	private void assertConsumable(Item item)
	{
		assertTrue(item.getConsumable().isConsumable());
		assertTrue(item.getConsumable().canConsume(null));
	}

	private void assertUnequippable(Item item)
	{
		assertSame(NotEquippable.INSTANCE, item.getEquippable());
		assertFalse(item.getEquippable().isEquippable());
		assertFalse(item.getEquippable().canEquip(null));
	}

	private void assertEquippable(Item item)
	{
		assertTrue(item.getEquippable().isEquippable());
		assertTrue(item.getEquippable().canEquip(null));
	}

	@Test
	public void testProvisionsEffect()
	{
		assertEquals(15, mike.getCurrentStamina());
		GameRules.takeDamage(mike, 5);
		assertEquals(10, mike.getCurrentStamina());
		Item provisions = reader.items.get(1);
		assertEquals("Provisions", provisions.getName());
		assertTrue(provisions.getConsumable().canConsume(mike));
		provisions.getConsumable().consume(mike);
		assertEquals(14, mike.getCurrentStamina());
	}

	@Test
	public void makeItemEquippableConsumable()
	{
		TestItem ti = new TestItem(0, "Fork", "A non equippable item.", Type.TREASURE);
		assertUnconsumable(ti);
		assertUnequippable(ti);
		// You can now defend yourself with a fork... Good luck!
		ti.setEquippable(GameRules.DEFAULT_EQUIPPABLE);
		assertUnconsumable(ti);
		assertEquippable(ti);
		// If you eat a fork, that will hurt you...
		ti.setConsumable(new DefaultConsumable(2, GameRules.RETRIEVE_STAMINA, GameRules.HARM));
		assertConsumable(ti);
		assertEquippable(ti);
	}

	class TestItem extends Item
	{
		public TestItem(int id, String name, String description, Type type)
		{
			super(id, name, description, type);
		}

		@Override
		public void setConsumable(Consumable consumable)
		{
			super.setConsumable(consumable);
		}

		@Override
		public void setEquippable(Equippable equippable)
		{
			super.setEquippable(equippable);
		}
	}
}
