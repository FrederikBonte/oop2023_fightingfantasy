package nl.frederikbonte.saxion.fighting;

import nl.frederikbonte.saxion.fighting.utils.AbstractIOSystem;
import nl.frederikbonte.saxion.fighting.utils.RandomSource;
import nl.frederikbonte.saxion.fighting.utils.ScriptedIOSystem;
import nl.frederikbonte.saxion.fighting.utils.StandardIOSystem;
import org.junit.jupiter.api.*;

import java.io.ByteArrayInputStream;
import java.util.Random;

import static org.junit.jupiter.api.Assertions.*;
public class TestBattle
{
	private Character character;

	class FixedRandomSource extends RandomSource {
		public static void reset(long seed)
		{
			FixedRandomSource.RNG = new Random(seed);
		}
	}

	@BeforeEach
	public void createCombatants()
	{
		character = new Character("Dead Mike", 8, 17, 8);
	}

	@Test
	public void testBattle1()
	{
		System.out.println("Battle agains the rats.");
		// Because of this seed we know exactly what all rolls will be.
		// That means we know when each combatant will do damage.
		FixedRandomSource.reset(12345);
		Character monster = new Character("Rats", 5, 5, 0);
		Battle battle = new Battle(createSIOS("y\nn\nn\n"), character, monster);
		assertBattleNotOver(battle, monster, 17,5,8);
		battle.doAttackRound();
		// Player hit, with luck failed.
		assertBattleNotOver(battle, monster, 17,1,7);
		battle.doAttackRound();
		// Player hit.
		assertBattleOver(battle, monster, 17,0,7);
	}

	@Test
	public void testBattle2()
	{
		System.out.println("Battle agains the orc.");
		// Because of this seed we know exactly what all rolls will be.
		// That means we know when each combatant will do damage.
		FixedRandomSource.reset(11113);
		Character monster = new Character("Orc Chieftan", 7, 8, 0);
		Battle battle = new Battle(createSIOS("y\ny\ny\ny\ny\nn\n"), character, monster);
		assertBattleNotOver(battle, monster, 17,8,8);
		battle.doAttackRound();
		// Monster hit, but lucky dodge.
		assertBattleNotOver(battle, monster, 16,8,7);
		battle.doAttackRound();
		// Player hit, plus extra damage.
		assertBattleNotOver(battle, monster, 16,4,6);
		battle.doAttackRound();
		// Player hit, no extra damage.
		assertBattleNotOver(battle, monster, 16,2,5);
		battle.doAttackRound();
		// Monster hit, full damage.
		assertBattleNotOver(battle, monster, 14,2,4);
		battle.doAttackRound();
		assertBattleOver(battle, monster, 14,0,4);
	}

	private void assertBattleNotOver(Battle battle, Character monster, int pStamina, int mStamina, int pLuck)
	{
		assertEquals(pStamina, character.getCurrentStamina());
		assertEquals(mStamina, monster.getCurrentStamina());
		assertEquals(pLuck, character.getCurrentLuck());
		assertFalse(battle.isOver());
	}

	private void assertBattleOver(Battle battle, Character monster, int pStamina, int mStamina, int pLuck)
	{
		assertEquals(pStamina, character.getCurrentStamina());
		assertEquals(mStamina, monster.getCurrentStamina());
		assertEquals(pLuck, character.getCurrentLuck());
		assertTrue(battle.isOver());
	}

	private AbstractIOSystem createSIOS(String s)
	{
		return new ScriptedIOSystem(new ByteArrayInputStream(s.getBytes()));
	}


}
